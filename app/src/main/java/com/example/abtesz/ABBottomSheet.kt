package com.example.abtesz

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.viewpager2.widget.ViewPager2
import com.example.abtesz.databinding.LayoutBottomSheetBinding
import com.google.android.material.bottomsheet.BottomSheetDialogFragment

class ABBottomSheet(val status: Boolean) : BottomSheetDialogFragment() {
    private lateinit var binding: LayoutBottomSheetBinding
    private lateinit var adapter: ABAdapter

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        super.onCreateView(inflater, container, savedInstanceState)

        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.layout_bottom_sheet,
            container,
            false
        )

        setBindingVariables()

        return binding.root
    }

    fun prev() {
        binding.viewPager.currentItem = binding.viewPager.currentItem.minus(1)
    }

    fun next() {
        binding.viewPager.currentItem = binding.viewPager.currentItem.plus(1)
    }

    private fun setBindingVariables() {
        binding.fragment = this

        adapter = ABAdapter()
        binding.viewPager.adapter = adapter
        binding.viewPager.orientation = ViewPager2.ORIENTATION_HORIZONTAL
    }
}