package com.example.abtesz

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.MutableLiveData
import com.example.abtesz.databinding.ActivityMainBinding
import com.google.firebase.installations.FirebaseInstallations
import com.google.firebase.ktx.Firebase
import com.google.firebase.remoteconfig.FirebaseRemoteConfig
import com.google.firebase.remoteconfig.ktx.get
import com.google.firebase.remoteconfig.ktx.remoteConfig
import com.google.firebase.remoteconfig.ktx.remoteConfigSettings

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private lateinit var remoteConfig: FirebaseRemoteConfig

    var status = MutableLiveData<Boolean>(false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.activity = this

        setABTest()
    }

    fun openBottomSheet() {
        val bottomSheet = ABBottomSheet(status.value ?: false)
        bottomSheet.show(supportFragmentManager, bottomSheet.tag)
    }

    private fun setABTest() {
        FirebaseInstallations.getInstance().getToken(true)
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Log.d("Installations", "Installation auth token: " + task.result?.token)
                }
            }

        remoteConfig = Firebase.remoteConfig

        val configSetting = remoteConfigSettings {
            minimumFetchIntervalInSeconds = 3600
        }

        remoteConfig.setConfigSettingsAsync(configSetting)
        remoteConfig.setDefaultsAsync(R.xml.remote_config_defaults)
        remoteConfig.fetchAndActivate().addOnCompleteListener {
            status.value = when (remoteConfig[BOTTOM_SHEET_BUTTON_ENABLE].asString()) {
                "true" -> {
                    Log.d("status", "true")
                    true
                }
                "false" -> {
                    Log.d("status", "false")
                    false
                }
                else -> {
                    Toast.makeText(this, "불러오기 실패", Toast.LENGTH_SHORT).show()
                    binding.btnBottomSheet.visibility = View.GONE
                    false
                }
            }
        }
    }

    companion object {
        private const val BOTTOM_SHEET_BUTTON_ENABLE = "bottom_sheet_button_enable"
    }
}