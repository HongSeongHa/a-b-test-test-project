package com.example.abtesz

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.abtesz.databinding.ItemAbBinding

class ABAdapter : RecyclerView.Adapter<ABAdapter.ABViewHolder>() {

    private val list = listOf(1, 2, 3, 4)

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ABViewHolder {
        val binding = ItemAbBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ABViewHolder(binding)
    }

    override fun getItemCount() = list.size

    override fun onBindViewHolder(holder: ABViewHolder, position: Int) {
        holder.bind(position)
    }

    inner class ABViewHolder(private val binding: ItemAbBinding) :
        RecyclerView.ViewHolder(binding.root) {

        fun bind(position: Int) {
            binding.textview.text = list[position].toString()
        }
    }
}